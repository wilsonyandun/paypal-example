//
//  ViewController.swift
//  PayPal Test
//
//  Created by administrator on 14/12/15.
//  Copyright © 2015 administrator. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PayPalPaymentDelegate {

    var payPalConfig = PayPalConfiguration()
    var environment:String = PayPalEnvironmentNoNetwork{
        willSet(newEnvironment){
            if(newEnvironment != environment){
            PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    var acceptCreditCards:Bool = true{
        didSet{
        payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        payPalConfig.acceptCreditCards = acceptCreditCards
        payPalConfig.merchantName = "Wilson Corp"
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "www.google.com")
        payPalConfig.merchantUserAgreementURL = NSURL(string: "www.google.com")
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0] as String
        payPalConfig.payPalShippingAddressOption = .PayPal;
        PayPalMobile.preconnectWithEnvironment(environment)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController!) {
        print("PayPal Payment Cancelled")
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController!, didCompletePayment completedPayment: PayPalPayment!) {
        print("PayPal Payment Success!")
        paymentViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n")
        })
    }

    @IBAction func payPressed(sender: AnyObject) {
        var item1 = PayPalItem(name: "product 1", withQuantity: 1, withPrice: NSDecimalNumber(string: "9.99"), withCurrency: "USD", withSku: "WilsonCorp1")
        let items = [item1]
        let subtotal = PayPalItem.totalPriceForItems(items)
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Wilson Corp Test", intent: .Sale)
        payment.items = items
        payment.paymentDetails = paymentDetails
        if(payment.processable){
        let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            presentViewController(paymentViewController, animated: true, completion: nil)
        }
        else{
        print("Payment not success \(payment)")
        }
        
        
    }

}

